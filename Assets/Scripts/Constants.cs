public class Constants {

    public static float BALLS_SPEED = 45f;
    public static float SCANNER_BELT_Y = 200f;
    public static float OUTPUT_BELT_X = 1350f;
    public static float DNR_SPAWN_TIME = 1.2f;

}
